import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { MaterialDashboardComponent } from './material-dashboard/material-dashboard.component';
const routes: Routes = [
  {
    path: 'a',
    component: AdminDashboardComponent
  },
  {
    path: 'm',
    component: MaterialDashboardComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
